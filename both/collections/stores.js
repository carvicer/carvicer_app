stores = new Mongo.Collection("Stores");

stores.attachSchema(new SimpleSchema ({
  ownerId: {
    type: String
  },
  yelpId:{
    type: String,
    optional:true
  },
  streetAddress: {
    type: String
  },
  city: {
    type: String,
    optional: true
  },
  state: {
    type: String,
    optional: true
  },
  licenceNo: {
    type: String,
    optional: true
  },
  authenticated: {
    type: Boolean,
    optional: true
  },
  scores: {
    type: String,
    optional: true
  },
  ratings: {
    type: String,
    optional: true
  },
  phoneNumber: {
    type: String,
    optional: true
  },
  assistantsId: {
    type: String,
    optional: true
  },
  services: {
    type:[Object],
    optional: true,
    blackbox:true
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnINsert: new Date};
      } else {
        this.unset();
      }
    },
  },
  updatedAt: {
    type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    }
	}
}));