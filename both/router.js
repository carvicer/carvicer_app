Ready = new Blaze.ReactiveVar(false);

Router.configure({
	layoutTemplate: 'layout'
});

Router.route('/', {
	action: function(){
		// if(activeOrders.count() != 0){
  //   		  	Session.set('reqActive', true);
  //   		  }else
  //   		  Session.set('reqActive', false);
		if(!Meteor.user()){
			this.layout('welcomeLayout');
			this.render('welcome');
		}else if(!Session.get('reqActive')){
			this.redirect("/customer_request");
		}else{
			this.redirect("/view_request");
		}
	},
	fastRender: true
});
Router.route('/signup', function(){
	if(!Meteor.user()){
		this.layout('welcomeLayout');
		this.render('signup');
	}
	else
		Router.go("/customer_request");
});

Router.route('/signup_terms',function(){
	if(!Meteor.user()){
		this.layout('signUpTermsLayout');
		this.render('signUpTerms');
	}
	else
		Router.go("/customer_request");
});

Router.route('/login', function(){
	if(!Meteor.user()){
		this.layout('welcomeLayout');
		this.render('login');
	}
	else
		Router.go("/customer_request");
});

Router.route('/customer_request', {
	subscriptions:function(){	
		return Meteor.subscribe('checkAnyActiveOrder', Meteor.userId());
	},
	action: function(){
		this.layout("submitRequestLayout");
		// this.layout("tabsLayout");
		this.render('customerRequest');
		if (this.ready()) {
    		  if (orders.find().count() != 0){
    		  	Session.set('reqActive', true);
    		  	Session.set("activeOrder", orders.findOne()._id);
				this.redirect("/view_request");
			}else{
				Session.set('reqActive', false);
				
			}
    	} else {
   //  	  	this.layout("tabsLayout");
			// this.render('tabsLoading');
    	}		
	},
	fastRender: true
});

Router.route('/view_request', {
	subscriptions: function(){
		Meteor.subscribe('checkAnyActiveOrder', Meteor.userId());
		Meteor.subscribe("getOffers", Meteor.userId());
	},
	action: function(){	
		Session.set('reqActive', true);
		
		this.layout("tabsLayout");
		this.render('viewRequest');
		if(this.ready)
			Session.set("activeOrder", orders.findOne()._id);

	}
});


Router.route('/active_request_detail',{
	action:function(){
		this.layout('tabsLayout');
		this.render("activeRequestDetail");
	},
	waitOn:function(){
		return Meteor.subscribe("checkAnyActiveOrder", Meteor.userId());
	}
});
Router.route('/active_request_update',{
	action:function(){
		this.layout('tabsLayout');
		this.render("activeRequestUpdate");
	},
	waitOn:function(){
		return Meteor.subscribe("checkAnyActiveOrder", Meteor.userId());
	}
});
Router.route('/offer_detail/:offerId', {
	action:function(){
		this.layout('tabsLayout');
		this.render("offerDetail");
	},
	subscriptions:function(){
		Meteor.subscribe("getOfferInfo", this.params.offerId);
	}
});
Router.route('/make_payment', {
	action:function(){
		this.layout('tabsLayout');
		this.render("makePayment");
	}
});
Router.route('/thank_payment', {
	action:function(){
		this.layout('submitRequestLayout');
		this.render("thankPayment");
	}
});
/* Search */
Router.route('/customer_search', { 
	action: function(){
		this.layout('tabsLayout');
		if(!Session.get('isMap'))
			this.render('customerSearch');
		else
			this.render('searchMap');
}
});
Router.route('/store_detail/:storeId', { 
	subscriptions: function(){
		return Meteor.subscribe("getStoreDetail", this.params.storeId);
	},
	action: function(){
		this.layout('tabsLayout');
		this.render('storeDetail');
}
});
/* Search END */

/* Account */
Router.route('/customer_account', {
	subscriptions: function(){
		return Meteor.subscribe("getAvatar",  Meteor.userId());
	},
	action: function(){
		this.layout('tabsLayout');
	this.render('customerAccount');
	}
	
});
Router.route('/edit_my_info',{
	subscriptions: function(){
		return Meteor.subscribe("getAvatar",  Meteor.userId());
	},
	action: function(){
		this.layout('tabsLayout');
		this.render('editMyInfo');
	}
});
Router.route('/edit_my_vehicle',{
	action: function(){
		this.layout('tabsLayout');
		this.render('editMyVehicle');
	}
});
Router.route('/edit_my_payment',function(){
	this.layout('tabsLayout');
	this.render('editMyPayment');
});
Router.route('/transaction_history',{
	subscriptions: function(){
		Meteor.subscribe("getTrans",Meteor.userId());
	},
	data: function(){
		return {trans:transactions.find({},{sort:{paid:1}})};
	},
	action:function(){
		if (this.ready()){
			this.layout('tabsLayout');
			this.render('transactionHistory');
			
		}
	}
});

Router.route('/transaction_detail/:_id',{
	subscriptions:function(){
		Meteor.subscribe("getTransDetail", this.params._id);
	},
	data: function(){
		return {trans:transactions.findOne()};
	},
	action:function(){
		if (this.ready()){
			this.render('transactionDetail');
			this.layout("tabsLayout");
		}
	}
});

Router.route('/view_notification',function(){
	this.layout('tabsLayout');
	this.render('viewNotification');
});
Router.route('/change_password',function(){
	this.layout('tabsLayout');
	this.render('changePassword');
});
/* Account END */

/* More */
Router.route('/customer_more', function(){
	this.layout('tabsLayout');
	this.render('customerMore');
});
Router.route('/bookmark', { 
	action: function(){
		this.layout('tabsLayout');
		if(!Session.get('isBookmarkMap'))
			this.render('bookmark');
		else
			this.render('bookmarkMap');
	}
});
Router.route('/promotions',function(){
	this.layout('tabsLayout');
	this.render('promotions');
});

Router.route('/terms_of_use',function(){
	this.layout('tabsLayout');
	this.render('termsOfUse');
});

Router.route('/privacy_policy',function(){
	this.layout('tabsLayout');
	this.render('privacyPolicy');
});

Router.route('/faq',function(){
	this.layout('tabsLayout');
	this.render('faq');
});

Router.route('/feedback_report',function(){
	this.layout('tabsLayout');
	this.render('feedbackReport');
});
/* More END */

Router.onBeforeAction(function(){
	var routeName = Router.current().route.getName();
	Session.set("currentTab", routeName);
	
	if(!Meteor.user() && routeName != "signup" && routeName != "login" && routeName != "signup_terms"){
		this.layout('welcomeLayout');
		this.render('welcome');
	}else{
		this.next();
	}
});

Router.onStop(function(){
	Session.set("previousPath",Router.current().route.getName());
});