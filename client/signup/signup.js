Session.set('signupStatus', 0);

var error;

Template.signup.events({
	'click [data-action = signup]': function(event, target){
		//perevent the defualt behaviour of form submitting
		event.preventDefault();
		var email = target.find('#email').value;
		var password = target.find('#password').value;
		var confirmation = target.find('#confirmation').value;
		var firstName = target.find('#first_name').value;
		var lastName = target.find('#last_name').value;
		var fullName = {
			firstName: firstName,
			lastName: lastName
		};
		console.log(fullName);
		//check if the passsword equals to confiramtion
		if (password != confirmation){
			Session.set('signupStatus', -1);
			return false;
		}
		Session.set("isLoading",true);
		//create users, let the server decide if the creation process goes well
		Accounts.createUser({
			email: email,
			password: password,
			profile: fullName
		}, function(err){
			if (err){
				error = err.message;
				Session.set('signupStatus', -2);
				console.log(err.message);
			}else{
				Session.set("isLoading",false);
				Session.set('signupStatus', 1);
				Session.set('loginStatus', 1);
				Router.go('/customer_request');
			}
		});
		return false;
	}
});

Template.signup.helpers({
	signupStatus: function(){
		switch(Session.get('signupStatus')){
			case -1:
				return "The password and confirmation don't match";
			case -2:
				return error;
			case 0:
				return "";
			default:
				return "";
		}
	}
});

Template.signup.onRendered(function(){
	Session.set("isLoading",false);
});