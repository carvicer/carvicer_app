//AutoForm.setDefaultTemplate('ionic');
Meteor.startup(function(){
  Session.setDefault('isLoading',false);
   Meteor.call("isProd",function(err,isProd){
    if(isProd)
      Stripe.setPublishableKey(Meteor.settings.public.stripeProdPublicKey);
    else
      Stripe.setPublishableKey(Meteor.settings.public.stripeTestPublicKey);
   });
   __meteor_runtime_config__.DDP_DEFAULT_CONNECTION_URL="https://carvicer.com:3004";
  //    Meteor.Spinner.options = {
  //     lines: 13, // The number of lines to draw
  //     length: 10, // The length of each line
  //     width: 5, // The line thickness
  //     radius: 15, // The radius of the inner circle
  //     corners: 0.7, // Corner roundness (0..1)
  //     rotate: 0, // The rotation offset
  //     direction: 1, // 1: clockwise, -1: counterclockwise
  //     color: '#fff', // #rgb or #rrggbb
  //     speed: 1, // Rounds per second
  //     trail: 60, // Afterglow percentage
  //     shadow: false, // Whether to render a shadow
  //     hwaccel: true, // Whether to use hardware acceleration
  //     className: 'spinner', // The CSS class to assign to the spinner
  //     zIndex: 2e9, // The z-index (defaults to 2000000000)
  //     top: 'auto', // Top position relative to parent in px
  //     left: 'auto' // Left position relative to parent in px
  // };

});


Template.registerHelper('getDistance',function(loc){
    var lat1 = Session.get('myLoc').latitude;
    var lon1 = Session.get('myLoc').longitude;
    var lat2 = loc.coordinates[1];
    var lon2 = loc.coordinates[0];
    var R = 3959; // 6371 for km
    var dLat = (lat2-lat1) * Math.PI / 180;
    var dLon = (lon2-lon1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
      Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return Math.round(d)+" mi";
});

// Push.addListener('token',function (token){
// 		alert(_.values(token)[0]);
// 	});

// Push.addListener('error', function(err) {
//         if (error.type == 'apn.cordova') {
//             alert(err.error);
//         }
//     });

//globle helper for adding index in an array
Template.registerHelper('addIndex', function(thatArray) {
  if (thatArray && thatArray.length) {
    $.each(thatArray, function (position, thatObject) {
      thatObject.index = position;
      thatArray[position] = thatObject;
    });
    return thatArray;
  }
});

Template.registerHelper('formatdate',function(datetime){
  if(moment && datetime){
    return moment(datetime).format('MM/DD/YYYY');
  } else{
    return datetime;
  }
});
 
Template.registerHelper('formatdatetime', function(datetime){
  if (moment && datetime) {
    if(datetime.getDate() === new Date().getDate()){
      return "Today " + moment(datetime).format("hh:mm");
    } else{
      return moment(datetime).format("MM/DD/YYYY hh:mm");
    }
 
  }
  else {
    return datetime;
  }
});
Template.body.helpers({
  loading:function(){
    return Session.get('isLoading');
  }
  });

AutoForm.hooks({
  updateUser: {
      // Called when form does not have a `type` attribute
      onSubmit: function(insertDoc, updateDoc, currentDoc) {
         if (customHandler(insertDoc)) {
             this.done();
           } else {
             this.done(new Error("Submission failed"));
           }
           return false;
         
        // You must call this.done()!
        //this.done(); // submitted successfully, call onSuccess
        //this.done(new Error('foo')); // failed to submit, call onError with the provided error
        //this.done(null, "foo"); // submitted successfully, call onSuccess with `result` arg set to "foo"
      },

      // Called when any submit operation succeeds
      onSuccess: function(formType, result) {
        IonPopup.show({
            title: 'Saved',
            template: 'Your information is saved',
            buttons: [
            {
              text: 'Ok',
              type: 'button-balanced',
              onTap: function() {
                IonPopup.close();
              }
            }]
        });        
      },


      // Called when any submit operation fails
      onError: function(formType, error) {
         IonPopup.show({
            title: 'Error',
            template: error.message,
            buttons: [
            {
              text: 'Ok',
              type: 'button-default',
              onTap: function() {
                IonPopup.close();
              }
            }]
        });
      },
       
  //     // Called every time an insert or typeless form
  //     // is revalidated, which can be often if keyup
  //     // validation is used.
  //     formToDoc: function(doc) {
  //       // alter doc
  //       // return doc;
  //     },

  //     // Called every time an update or typeless form
  //     // is revalidated, which can be often if keyup
  //     // validation is used.
  //     formToModifier: function(modifier) {
  //       // alter modifier
  //       // return modifier;
  //     },

  //     // Called whenever `doc` attribute reactively changes, before values
  //     // are set in the form fields.
  //     docToForm: function(doc, ss) {},

  //     // Called at the beginning and end of submission, respectively.
  //     // This is the place to disable/enable buttons or the form,
  //     // show/hide a "Please wait" message, etc. If these hooks are
  //     // not defined, then by default the submit button is disabled
  //     // during submission.
  //     beginSubmit: function() {},
  //     endSubmit: function() {}
   }
});