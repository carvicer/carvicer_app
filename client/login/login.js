Session.setDefault("loginStatus", 0);

Template.login.onRendered(function(){
	Session.set("loginStatus",0);
	Session.set("isLoading",false);
})

Template.login.events({
	//logic for login button is clicked
	'click [data-action = login]':function(event, template){
		event.preventDefault();
		Session.set("isLoading",true);
		//retreive the data
		var email = template.find('#email').value;
		var password = template.find('#password').value;
		
		//trim email and password
		//use meteor to validate the login 
		Meteor.loginWithPassword(email, password, function(err){
			Session.set("isLoading",false);
			if (err){
				Session.set("loginStatus", -1);
			}else{
				Session.set("loginStatus", 1);
				Router.go('/customer_request');
			}
		});
		return false;
	}
});

Template.login.helpers({
	equal: function(a, b){
		return a == b;
	},
	loginStatus: function(){
		if (Session.get("loginStatus") == 0)
			return "";
		else if (Session.get("loginStatus") == 1)
			return "LOGGED IN";
		else
			return "FAILED TO LOG IN";
	}
});