Session.setDefault("isMap", false);
Session.setDefault("searchService","");
Session.setDefault("searchCity","current");
Session.setDefault("searchRange",8000);

Template.ionNavBar.events({
	"click #change_map": function(event, target){
    	Session.set("isMap", true);

    	Router.go('/customer_search');
    	
    }
	
});
Template.customerSearch.onRendered(function(){
    $('#service').val(Session.get("searchService"));
    // $('#city').val(Session.get("searchCity"));
    // $('#range').val(Session.get("searchRange"));
});
Template.customerSearch.events({
    "change #service": function (event, target){
    	Session.set("searchService", $('#service').val());
    },
    "change #city": function (event, target){
    	Session.set("searchCity", $('#city').val());
    },
    "change #range": function (event, target){
    	Session.set("searchRange", $('#range').val());
    },


    "click #search":function(event, target){
    	if($('#service').val()==""){
    		Session.set("searchResult",{
    			total: 0
    		});
    	}else{
        Session.set("isLoading",true);
    	Meteor.call("getSearch",Session.get("searchService"),Session.get("searchCity"),Session.get("searchRange"),Session.get('myLoc'),function(err,result){
			Session.set("isLoading",false);
            if(err)
				console.log(err);
            else{
			 console.log(result);
			 Session.set("searchResult",result);
			}
		});
    	}
    },
    'click .item-store': function(event, target){
        Session.set('selectedStore', this.id);
    }
});
Template.customerSearch.helpers({
	chosenService: function(){
		return Session.get("searchService");
	},
    numberOfResults: function(){
        if(Session.get('searchResult')==null)
            return 0;
        else
            return Session.get('searchResult').total;
    },
	stores: function(){
		var output = Session.get('searchResult').businesses;
        output.sort(function (a,b){
            return b.carvicer - a.carvicer;
        });
        return output;
	},
    storePath: function(){
        return "/store_detail/" +this.id;
    }
});
Template.ionModal.helpers({
  barClass: function () {
    var classes = ['bar', 'bar-header', 'bar-energized'];

    return classes.join(' ');
  }
});
Template.ionModal.events({
    "change #city": function (event, target){
        Session.set("searchCity", $('#city').val());
    },
    "change #range": function (event, target){
        Session.set("searchRange", $('#range').val());
    },
    "click #search":function(event, target){
        if($('#service').val()==""){
            Session.set("searchResult",{
                total: 0
            });
        }else{
        Session.set("isLoading",true);
        Meteor.call("getSearch",Session.get("searchService"),Session.get("searchCity"),Session.get("searchRange"),Session.get('myLoc'),function(err,result){
            Session.set("isLoading",false);
            if(err)
                console.log(err);
            else{
            console.log(result);
            Session.set("searchResult",result);
        }   
            
        });
        }
    },
    'click  #search': function (event, template) {
    var tplName = getElementModalTemplateName(event.currentTarget);
    IonModal.close(tplName);
    }
});

var getElementModalTemplateName = function(element) {
  var modal = $(element).parents('.modal').get(0);
  var modalView = Blaze.getView(modal);
  var tplView = Meteor._get(modalView, 'parentView', 'parentView'); // Twice because the parent view is a #with block
  var tplName = tplView.name.slice('Template.'.length, tplView.name.length);
  return tplName;
};

