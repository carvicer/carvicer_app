Template.storeDetail.helpers({
	thisStore: function(){
		if(Session.get("previousPath")!="customer_search")
			return Session.get('thisOfferStore');
		else
			return Session.get('thisStore');
	},
	services:function(){
		return Session.get('storeServices');
	},
	previousPath: function(){
		return Session.get('previousPath');
	}
});

Template.storeDetail.events({
	"click #open_nav": function(event, template){
		var addr = $('#open_nav p').text();
		var queryAddr = addr.split(' ').join('+');
		//window.location.href = "maps://maps.apple.com/?q="+lat+","+lon;
		window.location.href = "maps://maps.apple.com/?q="+queryAddr;
	},
	"click .toggle_trigger":function(event,template){
    $(".toggle_trigger .my_store_list").next(".toggle_info").slideToggle("slow");
    }
});

Template.storeDetail.onRendered(function(){

	if(Session.get("previousPath")!="customer_search")
		var yelpId = Session.get('thisOfferStore').id;
	else{
		var allStores = Session.get('searchResult').businesses;
		allStores.forEach(function(store){
			if(store.id==Session.get('selectedStore'))
				Session.set("thisStore",store);
		});
		var yelpId = Session.get('selectedStore');
	}
	Meteor.call("getServiceList",yelpId,function (err,services){
		Session.set("storeServices",services);
	});
});