Template.customerSearch.onCreated(function(){
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
    
    function success(pos) {
      var crd = pos.coords;
        Session.set('myLat', crd.latitude);
        Session.set('myLng', crd.longitude);
    };
    
    function error(err) {
      console.warn('ERROR(' + err.code + '): ' + err.message);
    };

    navigator.geolocation.getCurrentPosition(success, error, options);
});

Template.searchMap.onRendered(function(){
    
  GoogleMaps.init(
    {
        //'sensor': true, //optional
        //'key': 'MY-GOOGLEMAPS-API-KEY', //optional
        //'language': 'de' //optional
    }, 
    function(){
        

        var mapOptions = {
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: false,
            streetViewControl: false,
            scaleControl: false,
            mapTypeControl: false
        };
        map = new google.maps.Map(document.getElementById("map"), mapOptions); 
        
        if(Session.get('searchResult') && Session.get("searchResult").businesses!=null){
            var results = Session.get("searchResult").businesses;
            var centerLat = Session.get("searchResult").businesses[0].location.coordinate.latitude;
            var centerLng = Session.get("searchResult").businesses[0].location.coordinate.longitude;
            var Latlng = new google.maps.LatLng(centerLat, centerLng);
            results.forEach(function (store,index,array){
                var infowindow = new google.maps.InfoWindow({
                    content: "<a href='/store_detail/"+store.id+"'>"+store.name+"</a><br /><img src='"+store.rating_img_url+"' />"
                });
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(store.location.coordinate.latitude, store.location.coordinate.longitude),
                    map: map,
                    title: 'Auto Service Store'
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);    
                });
                google.maps.event.addListener(marker, 'mouseout', function() {
                    infowindow.close(map,marker);    
                });
            });
        }else{
            Latlng =  new google.maps.LatLng(Session.get('myLat'), Session.get('myLng'));
        }
        map.setCenter(Latlng);
        map.setMyLocationEnabled(true);
        
    });
    this.autorun(function(){
        var bounds = map.getBounds();
        Session.set('mapBounds',bounds);
    });

  //  AdaptiveMaps.init(function () {
        
        // if(!Geolocation.error()){
        // 	var mapOptions = {
        // 	    mapTypeId: 'ROADMAP',
        //         mapTypeControl: false, 
        //         controls:{
        //             zoom:false,
        //             myLocationButton:true
        //         } ,
        //         camera:{ 
        // 	           latLng: new AdaptiveMaps.LatLng(Geolocation.latLng().lat, Geolocation.latLng().lng),
        // 	           zoom: 12
        //         },
        //         scaleControl: false,
        //         panControl: false,
        //         streetViewControl:false,
        //         icon:{
        //             size:1
        //         }
        //     };
        // }
        // var map = new AdaptiveMaps.Map(document.getElementById('map'), mapOptions);
        //       var results = Session.get("searchResult").find();
        //       console.log(results);
        //              map.addLiveMarkers({
        //            cursor:results,
        //            transform: function (doc) {                             
        //                return {
        //                    title: doc.storeName,
        //                    position: new AdaptiveMaps.LatLng(doc.loc.coordinates[1], doc.loc.coordinates[0]),
        //                    foo: doc.city,
        //                    visible: true
        //                }
        //            },
        //            onClick: function () {
        //            console.log('Bar: ' + this.get('foo'));
        //         }  
        //     }); 
   // });
        
});

Template.ionNavBar.events({
    "click #change_list": function(event, target){
        Session.set("isMap", false);
    }      
});