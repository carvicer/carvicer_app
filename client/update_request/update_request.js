Template.activeRequestUpdate.helpers({
	activeRequest: function(){
		Session.set("errorNotification","");

		var thisOrder = orders.findOne();
		var output = {};
		if (thisOrder == null){
			sAlert.error("No Active Order", {effect: 'bouncyflip', position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
			Meteor.setTimeout(function(){
				Router.go('/customer_request');
			},1000);
		}else{
			var preferredDate = thisOrder.date;
	
			//do the countdown part here
			var createdAt = thisOrder.createdAt;
			createdAt = moment(createdAt);	
			var expiredAt = thisOrder.expiredAt;
			expiredAt = moment(expiredAt, "YYYY-MM-DD HH:mm:ss ZZ");
			var timeLeft = expiredAt.diff(moment(), "minutes");
	
			if (timeLeft < 0){
				sAlert.error("Order has been expired", {effect: 'bouncyflip', position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
				Meteor.call("deactivateRequest", thisOrder._id);
				return container;
			}
	
			var hour, minute;
			if (timeLeft >= 60){
				hour = Math.floor(timeLeft/60);
				minute = timeLeft % 60;
				timeLeft = hour + "h " + minute +"m";
			}else
				timeLeft = timeLeft + "min";	
			output["vehicle"] = thisOrder.vehicle;
			output["service"] = thisOrder.service;
			output["date"] = moment(preferredDate).format("YYYY-MM-DD");
			output["createdAt"] = createdAt.format('MM/DD/YYYY, h:mm:ss a');
			output["expire"] = timeLeft;
			output["message"] = thisOrder.message;
			return output;
		}
	},
	myVehicles:function(){
		return Meteor.user().profile.vehicles;
	}
});

Template.activeRequestUpdate.onRendered(function(){
	Session.set("isLoading",false);
	//check the order is valid or not
	var thisOrder = orders.findOne();
	var createdAt = thisOrder.createdAt;
	createdAt = moment(createdAt, "YYYY-MM-DD HH:mm:ss ZZ");	
	var expiredAt = thisOrder.expiredAt;
	expiredAt = moment(expiredAt, "YYYY-MM-DD HH:mm:ss ZZ");
	var timeLeft = expiredAt.diff(moment(), "minutes");

	if (timeLeft < 0){
		Meteor.call("deactivateRequest", thisOrder._id,function(error,callback){
			Router.go('/view_request');	
		});
	}
	var thisOrder = orders.findOne();
	$.each(thisOrder.service,function(index,service){
		$(".service[value='"+service+"']").prop('checked',true);
	});
	$("#customer_vehicle").val(thisOrder.vehicle);
	
	//$(".service[value='Smog Check']").prop('checked',true);
});


Template.activeRequestUpdate.events({
	//logic for login button is clicked
	'submit #customer_update_form':function(event, template){
		//prevent default behaviour
		event.preventDefault();	
		Session.set("isLoading",true);
		var customer_vehicle = template.find("#customer_vehicle").value;
		var customer_date = template.find("#customer_service_date").value;
		var customer_requesting_time = template.find("#customer_requesting_time").value;
		var message = template.find("#message").value;
		var chosenServices =[];
		$.each($(".service:checked"),function(index,service){
			chosenServices.push(service.value);
		});
		if (chosenServices == "" || customer_vehicle == "" || customer_date == "" || customer_requesting_time == ""){
			sAlert.error("Please Fill in Every Blank", {position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
			return false;
		}
		//post it to the server
		
		var currentTime = moment().format("YYYY-MM-DD");
		var date = moment(customer_date, "YYYY-MM-DD");
		var offset = date.diff(currentTime, "days");
		if (offset < 0){
			sAlert.error("We Cant Travel Through Time", {position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
			return false;
		}
		//trnasform the date 
		date = moment().add(offset, 'days').format("YYYY-MM-DD Z");

		var thisOrder = orders.findOne();

		Meteor.call("updateOrder", thisOrder._id, chosenServices, customer_vehicle, date, customer_requesting_time, message, function(err, result){
			Session.set("isLoading",false);
			if(err){
				IonPopup.show({
      				title: 'Error',
      				template: err.reason,
      				buttons: [{
      				  text: 'Ok',
      				  type: 'button-default',
      				  onTap: function() {
      				    IonPopup.close();
      				  }
      				}]
    			});
			}else{
				IonPopup.show({
      				title: 'Notice',
      				template: 'Request update successful!',
      				buttons: [{
      				  text: 'Ok',
      				  type: 'button-balanced',
      				  onTap: function() {
      				    IonPopup.close();
      				    Router.go('/active_request_detail');
      				  }
      				}]
    			});
			}
			
		});
	},
	'click [data-action = cancelRequest]':function(event, template){
		Session.set("isLoading",true);
		Meteor.call("cancelRequest",orders.findOne()._id,function(err,callback){
			Session.set("isLoading",false);
			if(!err)
				Router.go('/customer_request');
		});
	}
});