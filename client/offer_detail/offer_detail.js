Template.offerDetail.onRendered(function(){
    $('input.cc-num').payment('formatCardNumber');
    $('input.cc-cvc').payment('formatCardCVC');
    $('input.cc-exp').payment('formatCardExpiry');
    if(!offers.findOne())
        var yelpId = Session.get("thisOfferStore").id;
    else
        var yelpId = offers.findOne().storeYelpId;
    Meteor.call('getThisYelpStore',yelpId, function(err,store){
        Session.set('thisOfferStore',store);
        GoogleMaps.init({
            //'sensor': true, //optional
            //'key': 'MY-GOOGLEMAPS-API-KEY', //optional
            //'language': 'de' //optional
        },function(){
                var mapOptions = {
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: false,
                streetViewControl: false,
                scaleControl: false,
                mapTypeControl: false
            };
            map = new google.maps.Map(document.getElementById("map"), mapOptions); 
            
            var thisStore = Session.get("thisOfferStore");

                var centerLat = thisStore.location.coordinate.latitude;
                var centerLng = thisStore.location.coordinate.longitude;
                var Latlng = new google.maps.LatLng(centerLat, centerLng);

                    var infowindow = new google.maps.InfoWindow({
                        content: "<p>"+thisStore.name+"</p><br /><img src='"+thisStore.rating_img_url+"' />"
                    });
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(thisStore.location.coordinate.latitude, thisStore.location.coordinate.longitude),
                        map: map,
                        title: 'Auto Service Store'
                    });
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map,marker);    
                    });
                    google.maps.event.addListener(marker, 'mouseout', function() {
                        infowindow.close(map,marker);    
                    });

            map.setCenter(Latlng);
        });
    });

    Meteor.call('retrieveCardList',function(err, cards){
        Session.set('userCards',cards.data);
    });
});

Template.offerDetail.helpers({
	oneOffer: function(){
		var thisOffer = offers.findOne();
        Session.set("selectedOffer",thisOffer);
		return thisOffer;
	},
    cards: function(){
        return Session.get('userCards');
    },
    thisStore: function(){
        return Session.get('thisOfferStore');
    }
});

Template.offerDetail.events({
	'click [data-action = acceptOffer]':function(e, template){
        e.preventDefault();
         IonPopup.show({
                    title: 'Accept Offer',
                    template: 'Are you sure to accept this offer?',
                    buttons: [
                    {
                      text: 'Ok',
                      type: 'button-balanced',
                      onTap: function() {
                        IonPopup.close();
                        Meteor.call('acceptOffer', Session.get('selectedOffer'),$("#choose_payment_card").val(), function(err,res){
                            if(err){
                                IonPopup.show({
                                    title: 'Error',
                                    template: err.reason,
                                    buttons: [
                                    {
                                      text: 'Accept',
                                      type: 'button-positive',
                                      onTap: function() {
                                        IonPopup.close();
                                      }
                                    }]
                                });
                            }else{
                                Session.set("reqActive",false);
                                    var offer = Session.get("selectedOffer");
                                    Meteor.call("paymentDone",offer._id, offer.orderId, function(){
                                        IonPopup.show({
                                            title: 'OK',
                                            template: 'Successful',
                                            buttons: [
                                            {
                                              text: 'Ok',
                                              type: 'button-balance',
                                              onTap: function() {
                                                IonPopup.close();
                                                Router.go("/thank_payment")
                                              }
                                            }]
                                        });
                                        
                                    });
                            }
                        });
                        
                      }
                    },
                    {
                      text: 'Cancel',
                      type: 'button-default',
                      onTap: function() {
                        IonPopup.close();
                      }
                    }]
                });
	},
    "change #choose_payment_card": function(event, template){
        if(event.target.value=="add card"){
            Session.set('ionTab.current', 'customer_account'); 
            Router.go('/edit_my_payment');
        }
    },
    "click #add_card":function(){
        Session.set('ionTab.current', 'customer_account'); 
    },
    "click #storeCard":function(){
        Router.go('/store_detail/'+Session.get('thisOfferStore').id);
    },
    "click #toggle_trigger_request":function(event,template){
    $("#toggle_trigger_request .my_offer_toggle").next(".toggle_info").slideToggle("slow");
    },
    "click #toggle_trigger_offer":function(event,template){
    $("#toggle_trigger_offer .my_offer_toggle").next(".toggle_info").slideToggle("slow");
    },
    "click #toggle_setYourPayment":function(event,template){
    $("#toggle_setPayment .setPayment").next(".toggle_info").slideToggle("slow");
    },
    "click #save_card": function(event,template){
        
        Stripe.card.createToken({
            number: $('.cc-num').val(),
              cvc: $('.cc-cvc').val(),
              name: $('.cc-name').val(),
              exp_month: $('.cc-exp').payment('cardExpiryVal').month,
              exp_year: $('.cc-exp').payment('cardExpiryVal').year,
              address_zip: $('.cc-zip').val()
        },function(status,res){
             
            if(res.error)
                IonPopup.show({
                    title: 'Error',
                    template: res.error.message,
                    buttons: [
                    {
                      text: 'Ok',
                      type: 'button-default',
                      onTap: function() {
                        IonPopup.close();
                      }
                    }]
                });
            else{
                Session.set("isLoading",true);
                Meteor.call("createCard",res.id, Meteor.user().emails[0].address ,function(err,res){
                    Session.set("isLoading",false);
                    if(err)
                        IonPopup.show({
                            title: 'Error',
                            template: err.reason,
                            buttons: [
                            {
                              text: 'Ok',
                              type: 'button-default',
                              onTap: function() {
                                IonPopup.close();
                              }
                            }]
                        });
                    else{
                        IonPopup.show({
                            title: 'Card Saved',
                            template: "You can now use the card you just saved",
                            buttons: [
                            {
                              text: 'Ok',
                              type: 'button-balanced',
                              onTap: function() {
                                IonPopup.close();
                                Meteor.call('retrieveCardList',function(err,cards){          
                                    Session.set('userCards',cards.data);
                                    $("#toggle_setPayment .setPayment").next(".toggle_info").slideToggle("slow"); 
                                });
                                 
                              }
                            }]
                        });
                    }
                });
            }
        });
    }
});