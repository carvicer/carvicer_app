Template.activeRequestDetail.helpers({
	activeRequest: function(){
		Session.set("errorNotification","");

		var thisOrder = orders.findOne();
		var output = {};
		if (thisOrder == null){
			sAlert.error("No Active Order", {effect: 'bouncyflip', position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
			Meteor.setTimeout(function(){
				Router.go('/customer_request');
			},1000);
		}else{
			var preferredDate = thisOrder.date;
	
			//do the countdown part here
			var createdAt = thisOrder.createdAt;
			createdAt = moment(createdAt);	
			var expiredAt = thisOrder.expiredAt;
			expiredAt = moment(expiredAt, "YYYY-MM-DD HH:mm:ss ZZ");
			var timeLeft = expiredAt.diff(moment(), "minutes");
	
			if (timeLeft < 0){
				sAlert.error("Order has been expired", {effect: 'bouncyflip', position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
				Meteor.call("cancelRequest", thisOrder._id);
				Meteor.setTimeout(function(){
					Router.go('/customer_request');
				},1000);
				return output;
			}
	
			var hour, minute;
			if (timeLeft >= 60){
				hour = Math.floor(timeLeft/60);
				minute = timeLeft % 60;
				timeLeft = hour + "h " + minute +"m";
			}else
				timeLeft = timeLeft + "min";
	
			output["vehicle"] = thisOrder.vehicle;
			output["service"] = thisOrder.service;
			output["date"] = moment(preferredDate,"YYYY-MM-DD").format("MM/DD/YYYY");
			output["createdAt"] = createdAt.format('MM/DD/YYYY, h:mm:ss a');
			output["expire"] = timeLeft;
	
	
			return output;
		}
	}
});