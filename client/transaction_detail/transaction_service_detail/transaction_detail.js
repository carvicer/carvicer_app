  Template.transactionDetail.helpers({

});

Template.transactionDetail.events({
	"click #confirm": function(event, template){
		IonPopup.show({
            title: 'Confirm',
            template: 'Are you sure to confirm this charge?',
            buttons: [{
              text: 'Confirm',
              type: 'button-balanced',
              onTap: function() {
              	Meteor.call('confirmCharge',event.target.value, function(err, res){
              		if(err){
              			IonPopup.close();
              			IonPopup.show({
      			     			title: 'Error',
      			     			template: err.message,
      			     			buttons: [{
      			     			  text: 'Ok',
      			     			  type: 'button-positive',
      			     			  onTap: function() {
      			     			    IonPopup.close();
      			     			  }
      			     			}] 
    				      	});
              		}else{
              			IonPopup.close();
              		}
				        });  
              }
            },{
              text: 'Cancel',
              type: 'button-default',
              onTap: function() {
                IonPopup.close();
              }
            }]
        });

	},
	"click #cancel": function(){
		IonPopup.show({
            title: 'Notice',
            template: 'If you want to cancel this order, please send an email to support@carvicer.com',
            buttons: [{
              text: 'ok',
              type: 'button-default',
              onTap: function() {
                IonPopup.close(); 
              }
            }]
        });
	}
})