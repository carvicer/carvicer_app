Template.customerRequest.onRendered(function(){
	Session.set("isLoading",false);
	var currentTime = moment().format("YYYY-MM-DD");
	$('#customer_service_date').val(currentTime);
	if(Meteor.user().profile.vehicles==""||Meteor.user().profile.vehicles==null){
		IonPopup.show({
      		title: 'Add your vehicles',
      		template: 'You haven\'t set your vehicles yet, go to edit your vehicles?',
      		buttons: [{
      		  text: 'Cancel',
      		  type: 'button-default',
      		  onTap: function() {
      		    IonPopup.close();
      		  }
      		},
      		{
      		  text: 'Go',
      		  type: 'button-balanced',
      		  onTap: function() {
      		  	IonPopup.close();
      		  	Session.set('ionTab.current', 'customer_account'); 
      		    Router.go('/edit_my_vehicle');
      		  }
      		}]
    	});
	}
	Location.locate(function(pos){
	   console.log("Got a position!", pos);
	   Session.set('myLoc',pos);
	}, function(err){
	   console.log("Oops! There was an error", err);
	});
});

Template.customerRequest.events({
	//add the event listener for the submit button
	"submit #customer_request_form": function(event, target){
		//prevent default behaviour
		event.preventDefault();
		//var customer_service = target.find("#customer_service").value;
		var customer_vehicle = target.find("#customer_vehicle").value;
		var customer_date = target.find("#customer_service_date").value;
		var message  =target.find("#message").value;
		var customer_requesting_time = target.find("#customer_requesting_time").value;
		
		var chosenServices =[];
		$.each($(".service:checked"),function(index,service){
			chosenServices.push(service.value);
		});
		//post it to the server
		if (chosenServices == "" || customer_vehicle == "" || customer_date == "" || customer_requesting_time == ""){
			sAlert.error("Please Fill in Every Blank", {position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
			return true;
		}
		var currentTime = moment().format("YYYY-MM-DD");
		var date = moment(customer_date, "YYYY-MM-DD");
		var offset = date.diff(currentTime, "days");
		if (offset < 0){

			sAlert.error("We Cant Travel Through Time", {position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
			return true;
		}
		//trnasform the date to utc 
		date = moment().add(offset, 'days').format("YYYY-MM-DD Z");
		if(Meteor.user().profile.phone){
			Session.set("isLoading",true);	
			Meteor.call("addOrders", chosenServices, customer_vehicle, date, customer_requesting_time, message, Session.get('myLoc'), function(error, result){
				Session.set("isLoading",false);
					if(err){
						sAlert.error("Send failed", {position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
					}else{
						HTTP.call('GET','http://localhost:1337/order/newReq?orderId='+result, function(){});
						Router.go('/view_request');
					}
			});
		}else{
			Session.set("isLoading",false);
			IonPopup.show({
      		title: 'Notice',
      		template: 'We highly recommand you to fill you phone number so that stores can contact with you',
      		buttons: [{
      		  text: 'Continue',
      		  type: 'button-default',
      		  onTap: function() {
      		    IonPopup.close();
      		    Session.set("isLoading",true);
				Meteor.call("addOrders", chosenServices, customer_vehicle, date, customer_requesting_time, message, Session.get('myLoc'), function(error, result){
					Session.set("isLoading",false);
					if(err){
						sAlert.error("Send failed", {position: 'top', timeout: '1000', onRouteClose: true, stack: false, offset: '0px'});
					}else{
						HTTP.call('GET','http://localhost:1337/order/newReq?orderId='+result, function(){});
						Router.go('/view_request');
					}
				});
      		  }
      		},
      		{
      		  text: 'Set',
      		  type: 'button-balanced',
      		  onTap: function() {
      		  	IonPopup.close();
      		  	Session.set('ionTab.current', 'customer_account'); 
      		    Router.go('/edit_my_info');
      		  }
      		}]
    	});
		}

	}
});

Template.customerRequest.helpers({
	myVehicles: function(){
		return Meteor.user().profile.vehicles;
	}
});