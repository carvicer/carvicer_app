Template.feedbackReport.events({
	"click #send_feedback": function(event,target){
		feedbacks.insert({
			userId: Meteor.user()._id,
			userEmail: Meteor.user().emails[0].address,
			message: target.find("#message").value,
			createdAt: moment().format()
		},function(err){
			if(!err)
			IonPopup.show({
		      				title: 'Sent',
		      				template: 'Thank you for your feedback!',
		      				buttons: [{
		      				  text: 'Ok',
		      				  type: 'button-balanced',
		      				  onTap: function() {
		      				    IonPopup.close();
		      				  }
		      				}]
		    			});			
		});
	}
});