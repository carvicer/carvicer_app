Template.customerMore.events({
	"click [data-action = signout]": function(){
		Session.set("isLoading",true);
		Meteor.logout(function(err){
			if (err)
				throw new Meteor.Error(403, "Cant logout");
			Object.keys(Session.keys).forEach(function(key){
				Session.set(key, undefined);
			});
			Session.keys = {};
			Session.set("ses",false);
			Session.set("isLoading",false);
			Router.go('/');
		});
	}
});