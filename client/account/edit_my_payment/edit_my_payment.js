Template.editMyPayment.onRendered(function (){
	$('input.cc-num').payment('formatCardNumber');
	$('input.cc-cvc').payment('formatCardCVC');
	$('input.cc-exp').payment('formatCardExpiry');
	Meteor.call('retrieveCardList',function(err,cards){
		Session.set('userCards',cards.data);
	});
});

Template.editMyPayment.events({
	"click #save_card": function(){
		Session.set("isLoading",true);
		Stripe.card.createToken({
			number: $('.cc-num').val(),
			  cvc: $('.cc-cvc').val(),
			  name: $('.cc-name').val(),
			  exp_month: $('.cc-exp').payment('cardExpiryVal').month,
			  exp_year: $('.cc-exp').payment('cardExpiryVal').year,
			  address_zip: $('.cc-zip').val()
		},function(status,res){

			if(res.error){
				Session.set("isLoading",true);
				IonPopup.show({
		      				title: 'Error',
		      				template: res.error.message,
		      				buttons: [{
		      				  text: 'Ok',
		      				  type: 'button-positive',
		      				  onTap: function() {
		      				    IonPopup.close();
		      				  }
		      				}]
		    			});
			}
			else{
				Meteor.call("createCard",res.id, Meteor.user().emails[0].address ,function(err,res){
					Session.set("isLoading",false);
					if(err){
						IonPopup.show({
		      				title: 'Errer',
		      				template: err.reason,
		      				buttons: [{
		      				  text: 'Ok',
		      				  type: 'button-positive',
		      				  onTap: function() {
		      				    IonPopup.close();
		      				  }
		      				}]
		    			});
					}
					else{
						IonPopup.show({
		      				title: 'Saved',
		      				template: 'Your card saved',
		      				buttons: [{
		      				  text: 'Ok',
		      				  type: 'button-balanced',
		      				  onTap: function() {
		      				    IonPopup.close();
		      				    $('.cc-num').val('');
								$('.cc-cvc').val('');
								$('.cc-name').val('');
								$('.cc-zip').val('');
		      				    Meteor.call('retrieveCardList',function(err,cards){
									Session.set('userCards',cards.data);
								});
		      				  }
		      				}]
		    			});
					}
				});
			}
		});
	},
	"click .delete_card": function(){
		Meteor.call("deleteCard", this.id, function(err, res){
			if(err){

			}else{
				Meteor.call('retrieveCardList',function(err,cards){
							Session.set('userCards',cards.data);
						});
			}
		});

	}
});

Template.editMyPayment.helpers({
	previousPath: function(){
		return Session.get('previousPath');
	},
	cards: function(){
		return Session.get('userCards');
	}
})