Template.customerAccount.events({

});

Template.customerAccount.helpers({
	firstName: function (){
		return Meteor.user().profile.firstName;
	},
	lastName: function (){
		return Meteor.user().profile.lastName;
	},
	email: function(){
		return Meteor.user().emails[0].address;
	},
	phone:function(){
		return Meteor.user().profile.phone;
	},
	avatar: function(){
		return Images.findOne(Meteor.user().profile.avatar);
	}
});