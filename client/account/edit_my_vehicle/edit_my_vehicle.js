Template.editMyVehicle.events({
	"change #choose_make": function (){
		if($('#choose_make').val() != ""){
			$('#choose_model').prop('disabled',true);
			$('#choose_year').prop('disabled',true);
			HTTP.call("GET", "https://api.edmunds.com/api/vehicle/v2/"+ $('#choose_make').val() +"?fmt=json&api_key=rnghyd2d5zp5p2gdpg2ppn5q", function(err, result){
				Session.set("modelList",result.data.models);
				$('#choose_model').val('');
				$('#choose_year').val('');
				$('#choose_model').prop('disabled',false);
			});
		}
	},
	"change #choose_model": function (){
		if($('#choose_model').val() != ""){
			$('#choose_year').prop('disabled',true);
			HTTP.call("GET", "https://api.edmunds.com/api/vehicle/v2/"+ $('#choose_make').val() +"/"+ $('#choose_model').val() +"?fmt=json&api_key=rnghyd2d5zp5p2gdpg2ppn5q", function(err, result){
				Session.set("yearList",result.data.years);
				$('#choose_year').val('');
				$('#choose_year').prop('disabled',false);
			});
		}
	},
	"click #add_car": function (){
		if($('#choose_make').val()!="" && $('#choose_model').val()!="" && $('#choose_year').val()!=""){
			var id=new Mongo.ObjectID();
			Meteor.users.update({_id: Meteor.userId()}, {
               	$push:{ "profile.vehicles": { 
               		"id": id.valueOf(),
               		"make": $('#choose_make option:selected').text(),
               		"model": $('#choose_model option:selected').text(),
               		"year": $('#choose_year option:selected').text()
               		}
              	}
            });
            $('#choose_make').val("");
			$('#choose_model').val("");
			$('#choose_year').val("");
			$('#choose_model').prop('disabled',true);
			$('#choose_year').prop('disabled',true);
		}
	},
	"click .delete_vehicle": function (event, template){
		// var vehicleId = this.id;
		// $("."+vehicleId).addClass('bounceOutRight');
		
		Meteor.users.update({_id: Meteor.userId()},{$pull:{ "profile.vehicles": { "id": this.id}}});
		// Meteor.setTimeout(function() {
		// 	Meteor.users.update({_id: Meteor.userId()},{$pull:{ "profile.vehicles": { "id": vehicleId}}});
		// }, 500,vehicleId);
		
	}
});

Template.editMyVehicle.onRendered(function (){
	$('#choose_make').val("");
	$('#choose_model').val("");
	$('#choose_year').val("");
	$('#choose_model').prop('disabled',true);
	$('#choose_year').prop('disabled',true);

});

Template.editMyVehicle.helpers({
	myVehicles: function(){
		return Meteor.user().profile.vehicles;
	},
	makes: function (){
		return Session.get("makeList");
	},
	models: function (){
		return Session.get("modelList");
	},
	years: function (){
		return Session.get("yearList");
	}
}); 