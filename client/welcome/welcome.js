
Template.welcome.events({
	'click [data-action = login]': function(event, template){
		Router.go('/login');
	},
	'click [data-action = signup]': function(event, template){
		Router.go('/signup');
	}
});
