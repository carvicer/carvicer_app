//validate the signup process
Accounts.onCreateUser(function (options, user){ //called bedore Accounts.validateNewUser
	var email = user.emails[0];
	if (!email)
		throw new Meteor.Error(403, "You must provide a non-empty email");
	
	//check if the email is unique
	if (Meteor.users.findOne({email:email}))
		throw new Meteor.Error(403, "A user with email " + email.address + " already exists");
	user.profile = options.profile ? options.profile : {};
	//user.services = options.services ? options.services : {};
	try{
		var createStripeUser  = Meteor.wrapAsync(stripe.customers.create,stripe.customers);
		var result = createStripeUser({email: email.address});
		user.services.stripeId = result.id;
		return user;
	}catch(e){
		throw new Meteor.Error(403,e.message);
	}
	

	
});

Accounts.validateNewUser(function (user){	

	//check if the email is empty
	

	
	// if (user.username == "admin" || user.username == "root")
	// 	throw new Meteor.Error(403, "Invalid username");

	// if (user.username == "")
	// 	throw new Meteor.Error(403, "Username cant be empty");

	return true;
});

Accounts.validateLoginAttempt(function(options){
	if (options.user)
		return true;
	else 
		return false;
});
