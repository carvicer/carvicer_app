Meteor.startup(function(){
  //Push.debug=true;
  process.env.MAIL_URL=Meteor.settings.emailSMTP;
  process.env.MOBILE_DDP_URL = 'https://carvicer.com:3004';
  process.env.MOBILE_ROOT_URL = 'https://hanzclare.com:3004';
});

Images.allow({
  insert:function(userId,project){
    return true;
  },
  update:function(userId,project,fields,modifier){
   return true;
  },
  remove:function(userId,project){
    return true;
  },
  download:function(){
    return true;
  }
});

feedbacks.allow({
  insert:function(userId,project){
    return true;
  }
});

// ContactsFS.allow({
//   insert: function() { return true },
//   update: function() {return true},
//   remove: function() { return false}
// });