if(process.env.NODE_ENV=="production")
			 stripe = Meteor.npmRequire('stripe')(Meteor.settings.stripeProdSecretKey);
		else
			stripe = Meteor.npmRequire('stripe')(Meteor.settings.stripeTestSecretKey);
			 //stripe = StripeAPI(Meteor.settings.stripeTestSecretKey);

yelp = Meteor.npmRequire('yelp').createClient({
  consumer_key: "pgerlfBhoLzqzs1kbc5Jjw", 
  consumer_secret: "cQTUzMjKOoe2fHf40TMbMZzgvWo",
  token: "ZAC0EU40QntQ3sSZWB16xeHYiSRcrHnX",
  token_secret: "m-41ik6VwgC7ghVUcmmV427tMDQ"
});

Meteor.methods({
	isProd: function(){
		if(process.env.NODE_ENV=="production")
			return true;
		else
			return false;
	},
	addOrders: function(customer_service, customer_vehicle, customer_date, customer_requesting_time, message, myLoc){
		//check if there is any order active
		var activeOrders = orders.findOne({userId:this.userId});
		if (activeOrders != null)
			return null;
		var currentTime = moment().format();
		var expiredAt = moment().add(customer_requesting_time,"minutes").format();

		//check the order duratioin is a valid one
		var offset = moment(expiredAt, "YYYY-MM-DD").diff(moment(customer_date, "YYYY-MM-DD"), "days");
		if (offset > 0){
			expiredAt = customer_date.substring(0,10) + "T23:59:59"+customer_date.substring(11,customer_date.length);
		}
		var loc = { type:"Point",
			coordinates:[myLoc.longitude,myLoc.latitude]};
		var orderId = orders.insert({
			userId: this.userId,
			loc: loc,
			service: customer_service,
			vehicle: customer_vehicle,
			date: customer_date,
			message: message,
			expiredAt: expiredAt,
			createdAt: currentTime,
			updatedAt: currentTime
		});

		return orderId;
	},

	updateOrder: function(orderId, customer_service, customer_vehicle, customer_date, extendedTime, message){



		//check if there is any order active
		var currentTime = moment().format();
		var preExpiredAt = orders.findOne({_id:orderId}).expiredAt;
		//set the new expiration date
		preExpiredAt = moment(preExpiredAt, "YYYY-MM-DD HH:mm:ss ZZ");
		var newExpiredAt = preExpiredAt.add(extendedTime, "minutes").format();

		//check the order duratioin is a valid one
		var offset = moment(newExpiredAt, "YYYY-MM-DD").diff(moment(customer_date, "YYYY-MM-DD"), "days");
		if (offset > 0){
			newExpiredAt = customer_date.substring(0,10) + "T23:59:59"+customer_date.substring(11,customer_date.length);
		}


		offers.update({orderId:orderId},{$set:{
			valid:false,
			service: customer_service,
			vehicle: customer_vehicle,
			serviceDate: customer_date,
			
		}
		});
		var orderId = orders.update({_id:orderId},{$set:{
			service: customer_service,
			vehicle: customer_vehicle,
			date: customer_date,
			message: message,
			expiredAt:  newExpiredAt,
			updatedAt: currentTime,
		}});
		return orderId;
	},
	cancelRequest:function(orderId){
		orders.remove({_id:orderId});
		offers.remove({orderId:orderId});
	},
	acceptOffer: function(offer, cardId){
		if(offer && offer.valid){
			transactions.insert({
				userId: offer.userId,
				userEmail: Meteor.user().emails[0].address,
				userName: offer.userName,
				paymentCard: cardId,
				storeId: offer.storeId,
				storeStripeId: offer.storeStripeId,
				storeName: offer.storeName,
				service: offer.service,
				vehicle: offer.vehicle,
				serviceDate: offer.serviceDate,
				price: offer.price,
				customerMsg:offer.customerMessage,
				storeMsg: offer.message,
				paid: false,
				canceled: false,
				createdAt:moment().format(),
			},function(err,trans){
				if(err)
					throw new Meteor.Error(403,"Offer not found");
				else{
					var thisTrans = transactions.findOne(trans);
					// var remote = DDP.connect('http://localhost:3001',{
					// 	onConnected: function() {
    	// 				  remote.call('pushOfferAccepted',thisTrans.storeId,function (err,result) {
    	// 				    console.dir(data);
    	// 				  });
    	// 				}
					// });
					var remote = DDP.connect('http://localhost:3001');
					remote.call('pushOfferAccepted',thisTrans.storeId,function (err,result){
						if(err)
							throw new Meteor.Error(400,err.message);
					});
					if(!thisTrans.customerMsg)
						thisTrans.customerMsg = "N/A";
					if(!thisTrans.storeMsg)
						thisTrans.storeMsg = "N/A";
					Email.send({
					    //to: Meteor.user().emails[0].address,
					    to:"lhz0516@hotmail.com",
					    from: 'Carvicer<support@carvicer.com>',
					    subject: 'Your Order has been placed!',
              			html: '<div style="width:540px;border:solid 1px black;"> <div class="header" style="width:540px;padding:60px 0 30px 0;display:inline-block;border-bottom:solid 1px black;text-align:center;"> <img src="https://carvicer.com:3004/logo2.jpg" style="width:50px;height:50px;border-radius:50%;margin-right:15px;"> <h2 style="-webkit-margin-before:0;-webkit-margin-after:0;padding:15px 0 5px 0;font-family:futura;">Your Order Has Been Placed</h2> <p style="-webkit-margin-before:3px;-webkit-margin-after:3px;padding-left:5px;font-family:avenir;">The following is your receipt.</p></div><div> <p style="font-family:avenir;padding:20px;">Thank you for using Carvicer. Here is your receipt number: <br><span style="font-weight:bold;font-style:italic;">'+thisTrans._id+'</span> <br><br><br>Customer Name: <span style="font-weight:bold;">Hanz Luo</span> <br><span style="font-size:14px;font-style:italic;">Vehicle:'+thisTrans.vehicle+'</span> <br>Message: <br><span style="font-size:14px;">'+thisTrans.customerMsg+'</span> <br><br>Store Name: <span style="font-weight:bold;">'+thisTrans.storeName+'</span> <br>Service: '+thisTrans.service+' <br>Price: '+thisTrans.price+' $ <br>Service Date: '+moment(thisTrans.serviceDate).format('MM/DD/YYYY')+'<br>Store Message: <br><span style="font-size:14px;">'+thisTrans.storeMsg+'</span> <br></p></div><div style="text-align:right;"> <h4 style="font-family:futura;padding-right:20px;">Carvicer.com</h4> </div></div>'
					  });
					return true;
				}
			});
		}else
			throw new Meteor.Error(403,"Offer doesn't exist or needs to be updated");
	},
	paymentDone:function(offerId, orderId){
		offers.update({_id:offerId},{$set:{accepted:true}});
		offers.remove({orderId:orderId,accepted:false});
		//offers.remove({$and:[{orderId:orderId},{accepted:false}]});
		orders.remove(orderId);
		return true;
	},
	confirmCharge: function(transId){
		var thisTrans = transactions.findOne(transId);
		stripe.charges.create({
		  amount: thisTrans.price*100,
		  currency: "usd",
		  customer: Meteor.user().services.stripeId, // obtained with Stripe.js
		  source: thisTrans.cardId,
		  description: thisTrans.services,
		  application_fee: 100,
		  destination: thisTrans.storeStripeId
		}, Meteor.bindEnvironment(function(err, charge) {
		  	if(err){
		  		console.log(err);
				throw new Meteor.Error(400,err.message);
		  	}
			else{
				transactions.update({_id:transId},{$set:{paid:true}});
			}
		}));
	},
	cancelChange: function(transId){
		transactions.update({_id:transId},{$set:{canceled:true}});
	},
	sendResetEmail: function(){
		Accounts.sendResetPasswordEmail(this.userId);
	},
	createCard: function(tok){	
		try{
			var createCard  = Meteor.wrapAsync(stripe.customers.createSource,stripe.customers);
			var result = createCard(Meteor.user().services.stripeId,{source: tok});
		}catch(err){
			throw new Meteor.Error(400,err.message);
		}
	},
	deleteCard: function(cardId){
		stripe.customers.deleteCard(
  		Meteor.user().services.stripeId,
  		cardId,
  		function(err, confirmation) {
  		  // asynchronously called
  		});
	},
	getSearch:function (service,city,range,loc){
		console.log(loc);		
		if(city=="current") 
			city="santa clara";
		var yelpSearch = Meteor.wrapAsync(yelp.search,yelp);
		var result = yelpSearch({term: service, location: city, radius_filter: range});
		var ids=[];
		var businesses =[];
		result.businesses.forEach(function(store){
			ids.push(store.id); //yelp Ids from search result
		});
		var cvStores = stores.find({yelpId:{$in:ids}}).fetch();
		if(cvStores=="")
			return result;
		else{
			cvStores.forEach(function(cvStore){
				result.businesses.forEach(function(store){
					if(store.id==cvStore.yelpId){
						store.carvicer = 1;
						businesses.push(store);
					}else{
						store.carvicer = 0;
						businesses.push(store);
					}
				});
			});
			
			var output ={};
			output.total = result.total;
			output.region = result.region;
			output.businesses = businesses;
			return output;
		}
	},
	getServiceList: function(yelpId){
		var thisStore = stores.findOne({yelpId:yelpId});
		if(thisStore)
			return thisStore.services;
	},
	getThisYelpStore: function(yelpId){
		var yelpBusiness = Meteor.wrapAsync(yelp.business,yelp);
		var result = yelpBusiness(yelpId);
		return result;
	}
});

Meteor.syncMethods({
	retrieveCardList: function(callback){
		stripe.customers.listCards(Meteor.user().services.stripeId, function(err,cards){
			callback(err,cards);
		});
	}
});
