Meteor.publish("orders", function(){
	return null;
});

Meteor.publish("getAvatar",function(id){
	
	return Images.find();
	//return Images.find(Meteor.users.findOne(id).profile.avatar);
});

Meteor.publish("offers", function(){
	return offers.find();
});

Meteor.publish("getNearbyStores", function(myLoc){
	var output = stores.find(
		 { loc:{
                $near:{
                    $geometry: {
                            type: "Point",
                            coordinates: myLoc
                    },
                    $maxDistance: 200000
                }
            }
        });
	return output;
});

Meteor.publish("getStoreDetail", function(id){
	console.log(id);
});
Meteor.publish("getTrans", function(userId){
	return transactions.find({userId: userId});
});
Meteor.publish("getTransDetail", function(id){
	return transactions.find(id);
});
Meteor.publish("getOffers", function(customerId){
	var output;
	var thisOrder = orders.findOne({userId:customerId});
	if (thisOrder == null)
		return output; 
	else{
		var orderId = thisOrder._id;
		output =  offers.find({orderId:orderId});
		return output;
	}
});

Meteor.publish("checkAnyActiveOrder", function(customerId){
	var output = orders.find({userId:customerId});
	//check if the order has expired or not
	if (output.count() != 0){
		var thisOrder;
		output.forEach(function(cursor){
			thisOrder = cursor;
		});
		var createdAt = thisOrder.createdAt;
		createdAt = moment(createdAt, "YYYY-MM-DD HH:mm:ss ZZ");	
		var expiredAt = thisOrder.expiredAt;
		expiredAt = moment(expiredAt, "YYYY-MM-DD HH:mm:ss ZZ");
		var timeLeft = expiredAt.diff(moment(), "minutes");
		if (timeLeft < 0){
			orders.remove(thisOrder._id);
			output = [];
			return output;
		}
	}

	return output;
});

Meteor.publish("getOfferInfo", function(offerId){
	var output = offers.find({_id:offerId});
	return output;
});

// Meteor.publish("getVehicles",function(userId){
// 	return;
// });

